var searchData=
[
  ['listaddend',['listAddEnd',['../list_8c.html#a1ac1ada1495ae1daba3eaa41c95325af',1,'listAddEnd(p_list *currentList, p_lcontact *newcontct):&#160;list.c'],['../list_8h.html#a72b0cb90fbd8042ddac6e5e205ef5bd2',1,'listAddEnd(p_list *, p_lcontact *):&#160;list.c']]],
  ['listaddfirst',['listAddFirst',['../list_8c.html#abad1a7333f4c1a1308199123ca1e9327',1,'listAddFirst(p_list *currentList, p_lcontact *newcontct):&#160;list.c'],['../list_8h.html#abf808327ed657d6c86f01bfdae96733f',1,'listAddFirst(p_list *, p_lcontact *):&#160;list.c']]],
  ['listinsertat',['listInsertAt',['../list_8c.html#ac728f21ab055b012c700051a6c39ecd6',1,'listInsertAt(p_list *currentList, p_lcontact *newcontct, int position):&#160;list.c'],['../list_8h.html#a9f2196f7dac1029e6ff8ed2dbfe82493',1,'listInsertAt(p_list *, p_lcontact *, int):&#160;list.c']]],
  ['listprint',['listPrint',['../list_8c.html#a1069716e50f68c18969bcfdf789d81e2',1,'listPrint(p_list *currentList):&#160;list.c'],['../list_8h.html#acd2d74b19b38fd670e0395b19d14b7ff',1,'listPrint(p_list *):&#160;list.c']]],
  ['listremove',['listRemove',['../list_8c.html#a734576ae6a7e1f6c78a54edebef4a403',1,'listRemove(p_list *currentList, p_lcontact *newcontct):&#160;list.c'],['../list_8h.html#a413df0aef94ea51bdd3777eb4c683c1e',1,'listRemove(p_list *, p_lcontact *):&#160;list.c']]]
];
