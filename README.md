# listedecontacts

## Auteurs
* **Mustapha Dahmani** - @ilyes_
* **Pierre Margale** - @pmp8
* **Bastien Pelmard** - @bastienpelmard
* **Ali Sookun** - @N-Ali/@N-Ali7


## Description

Listedecontacts est un projet permettant de gérer son répertoire, l'objectif de ce projet était de séparer les différents fichiers du code dans plusieurs dossiers.
Nous devions intégrer dans ce projet la recherche de contact , l'ajout et la supression.

## Exécution du projet

Pour pouvoir compiler il faut écrire sur un terminal les commandes suivantes 

```bash
make  
./listedecontacts
```


## Commandes

Voici les différentes commande à écrire pour pouvoir utiliser les fonctionnalités du projet
*  Recherche
*  Ajouter
*  Suppression
*  Affichage


