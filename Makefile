CC=gcc
CFLAGS= -W -Wall
SRC=src/
HEADERS=include/
BIN=bin/

all:listedecontacts

listedecontacts: contact.o list.o main.o fonctionnalites.o
	$(CC) $(CFLAGS) -o listedecontacts $(BIN)contact.o $(BIN)list.o $(BIN)main.o $(BIN)fonctionnalites.o

contact.o: $(SRC)contact.c $(HEADERS)contact.h
	$(CC) -c $(SRC)contact.c -o $(BIN)contact.o

list.o: $(SRC)list.c $(HEADERS)list.h
	$(CC) -c $(SRC)list.c -o $(BIN)list.o

fonctionnalites.o: $(SRC)fonctionnalites.c $(HEADERS)fonctionnalites.h
	$(CC) -c $(SRC)fonctionnalites.c -o $(BIN)fonctionnalites.o

main.o: $(SRC)main.c
	$(CC) -c $(SRC)main.c -o $(BIN)main.o

clean:
	rm $(BIN)*.o listedecontacts
