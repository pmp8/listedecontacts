/**
 * \file fonctionnalité.h
 * \brief Fichier header de fonctionnalites.c
 * \author Bastien Pelmard, Ali Sookun, Mustapha Dahmani, Pierre Margole
 * \version 1.0
 * \date 25 Octobre 2019
 *
 * Fichier pour les fonctiones prototypes.
 *
 */


/*prototypes*/
void clean(const char *, FILE *);
void fonctionAjout();
void fonctionAffichage();
void fonctionRecherche();
void fonctionSuppression();
void MiseAJourBDDSuppression();
void MiseAJourBDD();
void RecuperationInfoBDD();
