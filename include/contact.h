
/**
 * \file contact.h
 * \brief Header contact.
 * \author Bastien Pelmard, Ali Sookun, Mustapha Dahmani, Pierre Margole
 * \version 1.0
 * \date 25 Octobre 2019
 *
 * Header du fichier contact.c contenant la stucture p_lcontact permettant de créer un contact.
 *
 */
typedef struct p_lcontact p_lcontact;


/**
 * \struct p_lcontact
 * \brief Objet contenant chaines de caractères et un int.
 *
 * Str_t est un objet permettant de décrire un contact avec un nom, un prénom, un numéro de téléphone et un id unique.
 */
struct p_lcontact {
    int id;
    char* lastname;
    char* firstname;
    char* num;
};

/*prototypes*/
int compareContact(p_lcontact*, p_lcontact*);
p_lcontact* newContact(int, char*, char*, char*);
