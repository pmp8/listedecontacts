/**
 * \file list.h
 * \brief Header list.
 * \author Bastien Pelmard, Ali Sookun, Mustapha Dahmani, Pierre Margole
 * \version 1.0
 * \date 25 Octobre 2019
 *
 * Header du fichier list.c contenant les stucture p_node et p_list permettant de créer une liste qui contientra les différents contacts.
 *
 */


#include "contact.h"

/*This file is part of listedecontacts*/

typedef struct p_node p_node;
/**
 * \struct p_node
 * \brief Noeud de la liste.
 *
 * p_node est un objet qui a pour rôle d'être un noeud dans une liste. Il possède un contact et un lien qui le relie au contact 
 * précédent et suivant.
 */
struct p_node {
    p_lcontact* data; /*! La data d'un contact */
    struct p_node* next; /*! Pointeur qui va vers le contact suivant */
    struct p_node* prev; /*! Pointeur qui va vers le contact précédent */
};

typedef struct p_list p_list;
/**
 * \struct p_list
 * \brief Objet indiquant la taille, le début et la fin de p_node.
 *
 * p_list est un objet qui permet de connaitre le nombre de contact que l'on a et de trouver le premier ou le dernier contact 
 * directement.
 */

struct p_list {
    int length; /*! Taille de p_node soit le nombre de contact */
    struct p_node* tail; /*! Pointeur vers le premier élément de p_node */
    struct p_node* head; /*! Pointeur vers le dernier élément de p_node */
};

p_list* ListeG;


p_list* newList(void);
p_list* listAddEnd(p_list*, p_lcontact*);
p_list* listAddFirst(p_list*, p_lcontact*);
p_list* listInsertAt(p_list*, p_lcontact*, int);
p_list* listRemove(p_list*, p_lcontact*);
void listPrint(p_list*);


