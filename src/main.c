/**
 * \file ami,.c
 * \brief Fichier Main du projet.
 * \author Bastien Pelmard, Ali Sookun, Mustapha Dahmani, Pierre Margole
 * \version 1.0
 * \date 25 Octobre 2019
 *
 * Fichier possédant le main.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/list.h"
#include "../include/fonctionnalites.h"





/**
 * \fn main()
 * \brief Fonction main qui permet le lance du projet.
 *
 * \return 0 Ce qui indique que tout à fonctionné.
 */

int main(void) {
    ListeG = newList();
    
    
    RecuperationInfoBDD();
    
    char *requete = malloc(100*sizeof(char*));;
    printf("Bonjour, vous êtes dans votre liste de contact.\nPour rechercher un contact par son nom et prénom écrivez \"Recherche\"\nPour ajouter un contact écrivez \"Ajouter\"\nPour afficher les contacts écrivez \"Affichage\"\nPour supprimer un contact écrivez \"Suppression\"\nQue voulez-vous faire?\n");
    
    fgets(requete, 100, stdin);
    clean(requete, stdin);
    
    printf("requete=%s\n", requete);
    
    if(strcmp(requete, "Affichage") == 0){
        fonctionAffichage();
    }
    
    if(strcmp(requete, "Recherche") == 0){
        fonctionRecherche();
    }
    
    if(strcmp(requete, "Ajouter") == 0){
        listPrint(ListeG);
        fonctionAjout();
    }
    
    if(strcmp(requete, "Suppression") == 0){
        fonctionSuppression();
    }
    
    return 0;
}

