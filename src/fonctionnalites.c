/**
 * \file fonctionnalité.c
 * \brief Fichier Main du projet.
 * \author Bastien Pelmard, Ali Sookun, Mustapha Dahmani, Pierre Margole
 * \version 1.0
 * \date 25 Octobre 2019
 *
 * Fichier possédant les fonctionnalitées les plus importantes (base de données et modification de la liste)
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/fonctionnalites.h"
#include "../include/list.h"




/**
 * \fn clean(const char *buffer, FILE *fp)
 * \brief Fonction qui efface la chaine de caractères buffer du fichier fp.
 *
 * \param *buffer Chaine de caractères à effacer
 */


void clean(const char *buffer, FILE *fp){
    char *cherche_carac = strchr(buffer,'\n');
    if (cherche_carac != NULL)
        *cherche_carac = 0;
    else
    {
        int caractere;
        while ((caractere = fgetc(fp)) != '\n' && caractere != EOF);
    }
}



/**
 * \fn MiseAJourBDDSuppression()
 * \brief Fonction qui met à jour la base de données pour la suppression.
 *
 */

void MiseAJourBDDSuppression(){
    FILE* BaseDeDonnees = fopen("bdd/BaseDeDonnees.txt", "w+");
    
    if (ListeG != NULL) {
        p_node* tmpnode = ListeG->head;

        while (tmpnode != NULL) {
            fprintf(BaseDeDonnees, "Id: %d Num: %s Nom: %s Prénom: %s\n",tmpnode->data->id,
                    tmpnode->data->num, 
                    tmpnode->data->lastname, 
                    tmpnode->data->firstname);

            tmpnode = tmpnode->next;
        }
    }
    

    fclose(BaseDeDonnees);


}

/**
 * \fn MiseAJourBDD()
 * \brief Fonction qui met à jour la base de données.
 *
 */


void MiseAJourBDD(){
    FILE* BaseDeDonnees = fopen("bdd/BaseDeDonnees.txt", "a+");
    int une_fois = 0;
    
    if (ListeG != NULL && une_fois == 0) {
        p_node* tmpnode = ListeG->tail;
        
        while (tmpnode != NULL) {
            fprintf(BaseDeDonnees, "Id: %d Num: %s Nom: %s Prénom: %s\n",tmpnode->data->id,
                    tmpnode->data->num,
                    tmpnode->data->lastname,
                    tmpnode->data->firstname);
            
            tmpnode = tmpnode->next;
        }
        une_fois = 1;
    }
    
    fclose(BaseDeDonnees);
    
}

/**
 * \fn RecuperationInfoBDD()
 * \brief Fonction qui permet de récupper les informations de la base de données et de les mettres dans la listes.
 *
 */


void RecuperationInfoBDD(){
    FILE* BaseDeDonnees = NULL;
    BaseDeDonnees = fopen("bdd/BaseDeDonnees.txt","r");
    int id;
    char num[100], nom[100], prenom[100];
    
    
    if(BaseDeDonnees!=NULL){
        char df[128];
        while(fgets(df, sizeof(df), BaseDeDonnees) != NULL) {
            sscanf(df, "Id: %d Num: %s Nom: %s Prénom: %s", &id, num, nom, prenom);
            p_lcontact* nouveau_contact = newContact(id, num, nom, prenom);
            listAddEnd(ListeG, nouveau_contact);
        }
        
        listPrint(ListeG);
    }
    
    fclose(BaseDeDonnees);
}


/**
 * \fn fonctionAjout()
 * \brief Fonction qui ajoute un contact dans la base de données et met à jour la liste.
 *
 */


void fonctionAjout(){
    int id_du_contact = -1;
    char *id_string = malloc(100*sizeof(char*));;
    
    char *num_du_contact = malloc(100*sizeof(char*));;
    printf("Quel est le numéro de téléphone du contact que vous voulez rajouter?\n");
    fgets(num_du_contact, 100, stdin);
    clean(num_du_contact, stdin);
    
    char *nom_du_contact = malloc(100*sizeof(char*));;
    printf("Quel est le Nom du contact que vous voulez rajouter?\n");
    fgets(nom_du_contact, 100, stdin);
    clean(nom_du_contact, stdin);
    
    char *prenom_du_contact = malloc(100*sizeof(char*));;
    printf("Quel est le Prénom du contact que vous voulez rajouter?\n");
    fgets(prenom_du_contact, 100, stdin);
    clean(prenom_du_contact, stdin);
    
    FILE* BaseDeDonneesID = fopen("bdd/BaseDeDonneesID.txt", "a+");
    
    if(BaseDeDonneesID != NULL){
        fgets(id_string, 100, BaseDeDonneesID);
        printf("id string= %s\n", id_string);
    }
    
    id_du_contact = atoi(id_string) + 1;
    
    fclose(BaseDeDonneesID);
    
    
    BaseDeDonneesID = NULL;
    BaseDeDonneesID = fopen("bdd/BaseDeDonneesID.txt", "w+");
    
    fprintf(BaseDeDonneesID, "%d\n", id_du_contact);
    fclose(BaseDeDonneesID);
    
    p_lcontact* nouveau_contact = newContact(id_du_contact, num_du_contact, nom_du_contact, prenom_du_contact);
    
    ListeG = listAddEnd(ListeG, nouveau_contact);
    
    MiseAJourBDD();
    
    printf("Le contact a bien été ajouté à la liste des contacts.");
    
    
    
}

/**
 * \fn fonctionAffichage()
 * \brief Fonction qui affiche la liste de contact.
 *
 */


void fonctionAffichage(){
    printf("Affichage des contacts\n");
    FILE* BaseDeDonnees = NULL;
    BaseDeDonnees = fopen("bdd/BaseDeDonnees.txt","r");
    int id;
    char num[100];
    char nom[100];
    char prenom[100];
    
    if(BaseDeDonnees!=NULL){
        char df[128];
        while(fgets(df, sizeof(df), BaseDeDonnees) != NULL) {
            sscanf(df, "Id: %d Num: %s Nom: %s Prénom: %s", &id, num, nom, prenom);
            printf("Nom: %s Prénom: %s Num: %s\n", nom,prenom,num);
        }
    }
    fclose(BaseDeDonnees);
    
}

/**
 * \fn fonctionRecherche()
 * \brief Fonction qui qui permet de savoir si un contact est dans la liste des contacts en tappant un nom et un prénom.
 *
 */


void fonctionRecherche(){
    FILE* BaseDeDonnees = NULL;
    BaseDeDonnees = fopen("bdd/BaseDeDonnees.txt","r");
    int id;
    char num[100];
    char nom[100];
    char prenom[100];
    
    char *nom_du_contact = malloc(100*sizeof(char*));;
    printf("Quel est le Nom du contact que vous voulez rechercher?\n");
    fgets(nom_du_contact, 100, stdin);
    clean(nom_du_contact, stdin);
    
    char *prenom_du_contact = malloc(100*sizeof(char*));;
    printf("Quel est le Prénom du contact que vous voulez rechercher?\n");
    fgets(prenom_du_contact, 100, stdin);
    clean(prenom_du_contact, stdin);
    
    
    if(BaseDeDonnees != NULL){
        char df[128];
        while(fgets(df, sizeof(df), BaseDeDonnees) != NULL) {
            sscanf(df, "Id: %d Num: %s Nom: %s Prénom: %s", &id, num, nom, prenom);
            if((strcmp(nom_du_contact, nom) == 0) && (strcmp(prenom_du_contact, prenom) == 0)){
                printf("Nom: %s Prénom: %s Num: %s\n", nom,prenom,num);
                return;
            }
        }
    }
    printf("Le contact n'existe pas.\n");
    
}

/**
 * \fn fonctionSuppression()
 * \brief Fonction qui permet de supprimer un contact dans la liste.
 *
 */


void fonctionSuppression(){

    char *id_du_contact = malloc(100*sizeof(char*));;
    printf("Quel est l'id  du contact que vous voulez supprimer?\n");
    fgets(id_du_contact, 100, stdin);
    clean(id_du_contact, stdin);

    char *num_du_contact = malloc(100*sizeof(char*));;
    printf("Quel est le numéro du contact que vous voulez supprimer?\n");
    fgets(num_du_contact, 100, stdin);
    clean(num_du_contact, stdin);

    char *nom_du_contact = malloc(100*sizeof(char*));;
    printf("Quel est le nom du contact que vous voulez supprimer?\n");
    fgets(nom_du_contact, 100, stdin);
    clean(nom_du_contact, stdin);

    char *prenom_du_contact = malloc(100*sizeof(char*));;
    printf("Quel est le prénom du contact que vous voulez supprimer?\n");
    fgets(prenom_du_contact, 100, stdin);
    clean(prenom_du_contact, stdin);



    int id_du_contact_int = atoi(id_du_contact);


    p_lcontact* contact_a_sup = newContact(id_du_contact_int, num_du_contact, nom_du_contact, prenom_du_contact);
    ListeG = listRemove(ListeG, contact_a_sup);

    listPrint(ListeG);


    MiseAJourBDDSuppression();
}
