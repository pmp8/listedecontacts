/**
 * \file contact.c
 * \brief Fichier gérant les contacts.
 * \author Bastien Pelmard, Ali Sookun, Mustapha Dahmani, Pierre Margole
 * \version 1.0
 * \date 25 Octobre 2019
 *
 * Fichier permettant de comparer de comparer deux contacts et de créer un contact.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/contact.h"
#include "../include/fonctionnalites.h"


/**
 * \fn int compareContact(p_lcontact* c1, p_lcontact* c2)
 * \brief Fonction qui compare le nom et le prénom de deux contacts différents.
 *
 * \param c1 Contact 1 pour la comparaison.
 * \param c2 Contact 2 pour la comparaison.
 * \return 1 si les noms correspondent ou 0 sinon.
 */

int compareContact(p_lcontact* c1, p_lcontact* c2) {
    if (strcmp(c1->firstname, c2->firstname) == 0 && strcmp(c1->lastname, c2->lastname) == 0) { return 1; }
    else { return 0; }
}

/**
 * \fn p_lcontact* newContact(int id, char* num, char* lastname, char* firstname)
 * \brief Fonction qui crée un nouveau contact avec un id unique, un nom, un prénom et un numéro de téléphone.
 *
 * \param id Id unique du contact.
 * \param num Le numéro de téléphone du contact.
 * \param lastname Le nom de famille du contact.
 * \param lastname Le prenom du contact.
 * \return newcontct Le contact créé.
 */


p_lcontact* newContact(int id, char* num, char* lastname, char* firstname) {
    p_lcontact* newcontct = malloc(sizeof(p_lcontact));
    newcontct->id  = id;
    newcontct->firstname = malloc(sizeof(firstname) + 1);
    newcontct->lastname = malloc(sizeof(lastname) + 1);
    newcontct->num = malloc(sizeof(num) + 1);
    
    strcpy(newcontct->firstname, firstname);
    strcpy(newcontct->lastname, lastname);
    strcpy(newcontct->num, num);

    return newcontct;
}
