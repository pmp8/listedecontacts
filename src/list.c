/**
 * \file list.c
 * \brief Fichier gérant les listes.
 * \author Bastien Pelmard, Ali Sookun, Mustapha Dahmani, Pierre Margole
 * \version 1.0
 * \date 25 Octobre 2019
 *
 * Fichier permettant de créé, ajouter un éléments, de supprimer un élément ou encore d'afficher une liste.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/list.h"


/**
 * \fn p_list* newList(void)
 * \brief Fonction qui crée une liste vide.
 *
 * \return newl La liste vide que l'on vient de créer.
 */
p_list* newList(void) {
    p_list *newl = malloc(sizeof *newl);
    
    if (newl != NULL) {
        newl->length = 0;
        newl->head = NULL;
        newl->tail = NULL;
    }
    
    return newl;
}


/**
 * \fn p_list* listAddEnd(p_list* currentList, p_lcontact* newcontct)
 * \brief Fonction qui ajoute un élément contact à la fin de la liste.
 *
 * \param currentList La liste contenant tout les contacts.
 * \param newcontct Le contact que l'on va ajouter à la fin de notre liste.
 * \return currentList La liste avec son nouvel élément.
 */
p_list* listAddEnd(p_list* currentList, p_lcontact* newcontct) {
    if (currentList != NULL) { 
        p_node* newnode = malloc(sizeof(*newnode)); // alloue un nouveau noeud
        
        if (newnode != NULL) {
            newnode->data = newcontct;
            newnode->next = NULL;
            
            if (currentList->tail == NULL) { // cas où la liste est vide
                newnode->prev = NULL;
                currentList->head = newnode;
                currentList->tail = newnode;
                            
            } else { // cas où ya au moins un élément
                currentList->tail->next = newnode; // on lie le dernier élément actuel de la liste vers le nouveau élément
                newnode->prev = currentList->tail; // on fais pointer previous vers le dernier élément actuel de la liste
                currentList->tail = newnode; //on fais pointer la fin de notre liste vers le nouvelle élément
            }

            currentList->length++;
        }
    }

    return currentList;
}

/**
 * \fn p_list* listAddFirst(p_list* currentList, p_lcontact* newcontct)
 * \brief Fonction qui ajoute un élément contact au début de la liste.
 *
 * \param currentList La liste contenant tout les contacts.
 * \param newcontct Le contact que l'on va ajouter à la fin de notre liste.
 * \return currentList La liste avec son nouvel élément au début.
 */
p_list* listAddFirst(p_list* currentList, p_lcontact* newcontct) {
    if (currentList != NULL) {
        p_node* newnode = malloc(sizeof(*newnode));

        if (newnode != NULL) {
            newnode->data = newcontct;
            newnode->prev = NULL;

            if (currentList->tail == NULL) {
                newnode->next = NULL;
                currentList->head = newnode;
                currentList->tail = newnode;
            } else {
                currentList->head->prev = newnode;
                newnode->next = currentList->head;
                currentList->head = newnode;
            }

            currentList->length++;
        }
    }
    
    return currentList;
}

/**
 * \fn p_list* listInsertAt(p_list* currentList, p_lcontact* newcontct, int position)
 * \brief Fonction qui ajoute un élément contact au début de la liste.
 *
 * \param currentList La liste contenant tout les contacts.
 * \param newcontct Le contact que l'on va ajouter à la fin de notre liste.
 * \param position Un entier qui indique à quelle postion on ajoute le nouvel élément dans la liste.
 * \return currentList La liste avec son nouvel élément au début.
 */
p_list* listInsertAt(p_list* currentList, p_lcontact* newcontct, int position) {
    if (currentList != NULL) {
        p_node* tmpnode = currentList->head;
        int i = 1;

        while (tmpnode != NULL && i <= position) {
            if (position == i ) {
                if (tmpnode->next == NULL) {
                    currentList = listAddEnd(currentList, newcontct);
                }
                
                else if (tmpnode->prev == NULL) {
                    currentList = listAddFirst(currentList, newcontct);
                }

                else {
                    p_node* newnode = malloc(sizeof(*newnode));

                    if (newnode != NULL) {
                        newnode->data = newcontct;
                        tmpnode->next->prev = newnode;
                        tmpnode->prev->next = newnode;
                        newnode->prev = tmpnode->prev;
                        newnode->next = tmpnode;
                        currentList->length++;
                    }
                }
            }

            else {
                tmpnode = tmpnode->next;
            }
            
            i++;
        }
    }

    return currentList;
}


/**
 * \fn p_list* listRemove(p_list* currentList, p_lcontact* newcontct,)
 * \brief Fonction qui supprime un contact dans la liste qui correspond au contact mit en paramètre.
 *
 * \param currentList La liste contenant tout les contacts.
 * \param newcontct Le contact que l'on va supprimer notre liste.
 * \return currentList La liste avec l'élément en moins.
 */
p_list* listRemove(p_list* currentList, p_lcontact* newcontct) {
    
    if (currentList != NULL) {
        p_node* tmpnode = currentList->head;

        while (tmpnode != NULL) {
            if (compareContact(tmpnode->data, newcontct) == 1) {
                if (tmpnode->next == NULL) {
                    currentList->tail = tmpnode->prev;
                    currentList->tail->next = NULL;
                }

                else if (tmpnode->prev == NULL) {
                    currentList->head = tmpnode->next;
                    currentList->head->prev = NULL;
                }

                else {
                    tmpnode->next->prev = tmpnode->prev;
                    tmpnode->prev->next = tmpnode->next;
                }

                free(tmpnode);
                currentList->length--;
                break;
            }

            else {
                tmpnode = tmpnode->next;
            }
        }
        
        
    }
    

    return currentList;
}

/**
 * \fn listPrint(p_list* currentList)
 * \brief Fonction qui affiche la liste des contacts.
 *
 * \param currentList La liste contenant tout les contacts.
 */
void listPrint(p_list* currentList) {
    if (currentList != NULL) {
        p_node* tmpnode = currentList->head;

        while (tmpnode != NULL) {
            printf("(%d, %s, %s, %s) -> ",tmpnode->data->id,
                    tmpnode->data->firstname, 
                    tmpnode->data->lastname, 
                    tmpnode->data->num);

            tmpnode = tmpnode->next;
        }
    }

    printf("NULL\n");

    return;
}
